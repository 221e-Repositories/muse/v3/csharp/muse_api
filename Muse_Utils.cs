﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.NetworkInformation;
using static _221e.Muse.Muse_HW;

namespace _221e.Muse
{
    /// <summary>Utilities related to communication protocol specifications.</summary>
    public class Muse_Utils
    {
        #region USB WRAPPER FUNCTIONS

        /// <summary>Adds header and trailer to a standard command.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="buffer">Input byte array to be wrapped.</param>
        private static byte[] WrapMessage(byte[] buffer)
        {
            byte[] wrapped_buffer = null;
            if (buffer != null)
            {
                // Define wrapped message buffer
                wrapped_buffer = new byte[buffer.Length + 4];

                // Add header to input buffer
                wrapped_buffer[0] = (byte)'?';
                wrapped_buffer[1] = (byte)'!';

                // Copy main content
                Array.Copy(buffer, 0, wrapped_buffer, 2, buffer.Length);

                // Add trailer to input buffer
                wrapped_buffer[wrapped_buffer.Length - 2] = (byte)'!';
                wrapped_buffer[wrapped_buffer.Length - 1] = (byte)'?';
            }
            return wrapped_buffer;
        }

        /// <summary>Removes header and trailer from a standard command.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="buffer">Input byte array from which the header and trailer must be removed.</param>
        private static byte[] ExtractMessage(byte[] buffer)
        {
            // Remove header and trailer from input buffer
            byte[] dewrapped_buffer = null;
            if (buffer != null)
                Array.Copy(buffer, 2, dewrapped_buffer, 0, buffer.Length - 4);
            return dewrapped_buffer;
        }

        #endregion

        #region ENCODING COMMAND IMPLEMENTATION

        // CMD_ACK = 0x00
        /*
        public static byte[] Cmd_Acknowledge(AcknowledgeType ack)
        {
            // Definition of message buffer
            byte[] buffer = new byte[COMM_MESSAGE_LEN_CMD];
            return buffer;
        }
        */

        /// <summary>Builds command to retrieve current system state.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_GetSystemState(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_STATE];

            // Get state command
            buffer[0] = (byte)Command.CMD_STATE + READ_BIT_MASK;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to start acquisition in stream mode.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="mode">Identifies the set of data to be acquired.</param>
        /// <param name="frequency">Identifies the data acquisition frequency.</param>
        /// <param name="enableDirect">Allows to select the stream type (i.e., direct or buffered). </param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_StartStream(DataMode mode, DataFrequency frequency, bool enableDirect = false, CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_START_STREAM];

            // Start stream acquisition using set state command
            buffer[0] = (byte)Command.CMD_STATE;

            // Set payload length
            buffer[1] = (byte)CommandLength.CMD_LENGTH_START_STREAM - 2;

            // Set tx type based on boolean flag value
            if (enableDirect)
                buffer[2] = (byte)SystemState.SYS_TX_DIRECT;
            else
                buffer[2] = (byte)SystemState.SYS_TX_BUFFERED;

            // Set acquisition mode
            byte[] tmp = new byte[4];
            tmp = BitConverter.GetBytes((UInt32)mode);
            Array.Copy(tmp, 0, buffer, 3, 3);

            // Set acquisition frequency
            buffer[6] = (byte)frequency;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to start acquisition in log mode.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="mode">Identifies the set of data to be acquired.</param>
        /// <param name="frequency">Identifies the data acquisition frequency.</param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_StartLog(DataMode mode, DataFrequency frequency, CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_START_LOG];

            // Start stream acquisition using set state command
            buffer[0] = (byte)Command.CMD_STATE;

            // Set payload length
            buffer[1] = (byte)CommandLength.CMD_LENGTH_START_STREAM - 2;

            // Set state - LOG
            buffer[2] = (byte)SystemState.SYS_LOG;

            // Set acquisition mode
            byte[] tmp = new byte[4];
            tmp = BitConverter.GetBytes((UInt32)mode);
            Array.Copy(tmp, 0, buffer, 3, 3);

            // Set acquisition frequency
            buffer[6] = (byte)frequency;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to stop any data acquisition procedure.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_StopAcquisition(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_STOP_ACQUISITION];

            // Set IDLE state to stop any acquisition procedure
            buffer[0] = (byte)Command.CMD_STATE;
            buffer[1] = (byte)CommandLength.CMD_LENGTH_STOP_ACQUISITION - 2;
            buffer[2] = (byte)SystemState.SYS_IDLE;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to set restart device.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="restartMode">Allows to select the restart mode (i.e., reset or boot).</param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_Restart(RestartMode restartMode, CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_RESTART];
            
            // Set RESTART command with the specified restart mode (i.e., boot or app)
            buffer[0] = (byte)Command.CMD_RESTART;
            buffer[1] = (byte)CommandLength.CMD_LENGTH_RESTART - 2;
            buffer[2] = (byte)restartMode;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to retrive application firmware information.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_GetApplicationInfo(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_APP_INFO];

            // Get firmware application info
            buffer[0] = (byte)Command.CMD_APP_INFO + READ_BIT_MASK;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to retrieve battery charge level [%].</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_GetBatteryCharge(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_BATTERY_CHARGE];

            // Get battery charge
            buffer[0] = (byte)Command.CMD_BATTERY_CHARGE + READ_BIT_MASK;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to retrieve battery voltage level [mV].</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_GetBatteryVoltage(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_BATTERY_VOLTAGE];

            // Get battery voltage
            buffer[0] = (byte)Command.CMD_BATTERY_VOLTAGE + READ_BIT_MASK;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to retrieve system check-up register value.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_GetDeviceCheckup(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_CHECK_UP];

            // Get check up register value
            buffer[0] = (byte)Command.CMD_CHECK_UP + READ_BIT_MASK;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to retrieve current firmware versions (i.e., both bootloader and application).</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_GetFirmwareVersion(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_FW_VERSION];

            // Get firmware version labels (i.e., bootloader and application firmware)
            buffer[0] = (byte)Command.CMD_FW_VERSION + READ_BIT_MASK;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to update date/time.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_SetTime(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_SET_TIME];

            // Set time using current timespan since 1970
            buffer[0] = (byte)Command.CMD_TIME;
            buffer[1] = (byte)CommandLength.CMD_LENGTH_SET_TIME - 2;

            // Get current timespan since 1/1/1970
            TimeSpan timeSpan = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
            uint secondsSinceEpoch = (uint)timeSpan.TotalSeconds;

            // Set payload - seconds since epoch (4 bytes)
            byte[] payload = BitConverter.GetBytes(secondsSinceEpoch);
            Array.Copy(payload, 0, buffer, 2, (int)CommandLength.CMD_LENGTH_SET_TIME - 2);

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to retrieve current date/time.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_GetTime(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_TIME];

            // Get current datetime
            buffer[0] = (byte)Command.CMD_TIME + READ_BIT_MASK;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to retrieve device name (i.e., the name advertised by the Bluetooth module).</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_GetDeviceName(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_BLE_NAME];

            // Get device name (i.e., ble name)
            buffer[0] = (byte)Command.CMD_BLE_NAME + READ_BIT_MASK;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to change the device name (i.e., the name advertised by the Bluetooth module).</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_SetDeviceName(string bleName, CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_SET_BLE_NAME];

            // Check bleName to be set consistency before proceeding
            if (bleName != "" && bleName.Length < 16)
            {
                buffer[0] = (byte)Command.CMD_BLE_NAME;

                int respLen = bleName.Length;
                buffer[1] = Convert.ToByte(respLen);

                char[] tmpName = bleName.ToCharArray();
                for (int i = 0; i < respLen; i++)
                    buffer[2 + i] = Convert.ToByte(tmpName[i]);
            }

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to retrieve the device unique identifier.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_GetDeviceID(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_DEVICE_ID];

            // Get device unique identifier
            buffer[0] = (byte)Command.CMD_DEVICE_ID + READ_BIT_MASK;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to retrieve the devices skills (i.e. hardware or software features provided by the device). By default hardware skills are provided.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="softwareSkills">Allows to select the type of features of interest enabling or disabling software skills flag.</param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_GetDeviceSkills(bool softwareSkills = false, CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_DEVICE_SKILLS];

            // Get device skills (i.e., hardware or software based on specified flag)
            buffer[0] = (byte)Command.CMD_DEVICE_SKILLS + READ_BIT_MASK;
            buffer[1] = (byte)CommandLength.CMD_LENGTH_GET_DEVICE_SKILLS - 2;

            if (softwareSkills)
                buffer[2] = 0x01;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to retrieve current memory status.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_GetMemoryStatus(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_MEM_CONTROL];

            // Get available memory
            buffer[0] = (byte)Command.CMD_MEM_CONTROL + READ_BIT_MASK;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to erase device memory.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="bulk_erase"></param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_EraseMemory(byte bulk_erase = 0, CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_SET_MEM_CONTROL];

            // Erase memory 
            buffer[0] = (byte)Command.CMD_MEM_CONTROL;
            buffer[1] = (byte)CommandLength.CMD_LENGTH_SET_MEM_CONTROL - 2;
            buffer[2] = bulk_erase;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to retrieve file information.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="fileId">File identifier.</param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_MemoryFileInfo(int fileId, CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_MEM_FILE_INFO];

            // Retrieve file information given a specific file identifier
            buffer[0] = (byte)Command.CMD_MEM_FILE_INFO + READ_BIT_MASK;
            buffer[1] = (byte)CommandLength.CMD_LENGTH_GET_MEM_FILE_INFO - 2;

            // Set file id as a 2-bytes unsigned integer value
            byte[] valueBytes = BitConverter.GetBytes(Convert.ToInt16(fileId));
            buffer[2] = valueBytes[0];
            buffer[3] = valueBytes[1];

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to activate a memory file offload procedure.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="fileId">File identifier.</param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_MemoryFileDownload(int fileId, CommunicationChannel channel = CommunicationChannel.CHANNEL_USB)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_MEM_FILE_DOWNLOAD];

            // Start file offload procedure
            buffer[0] = (byte)Command.CMD_MEM_FILE_DOWNLOAD;
            buffer[1] = (byte)CommandLength.CMD_LENGTH_GET_MEM_FILE_DOWNLOAD - 2;

            // Set file identifier
            byte[] valueBytes = BitConverter.GetBytes(Convert.ToInt16(fileId));
            buffer[2] = valueBytes[0];
            buffer[3] = valueBytes[1];

            // Set file offload channel (USB vs BLE)
            buffer[4] = (byte)channel;
            if (channel == Muse_HW.CommunicationChannel.CHANNEL_USB)
                buffer[4] = 0x00; // set by default to USB channel (if 0x01 it manages the BLE transfer)

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to retrieve current clock offset.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_GetClockOffset(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_CLK_OFFSET];

            // Get clock offset
            buffer[0] = (byte)Command.CMD_CLK_OFFSET + READ_BIT_MASK;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to trigger a clock offset estimation procedure.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="inOffset">Allows to specify a custom clock offset to be set.</param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_SetClockOffset(double inOffset = 0, CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_SET_CLK_OFFSET];

            // Set clock offset
            buffer[0] = (byte)Command.CMD_CLK_OFFSET;
            buffer[1] = (byte)CommandLength.CMD_LENGTH_SET_CLK_OFFSET - 2;

            byte[] valueBytes = BitConverter.GetBytes(Convert.ToUInt64(inOffset));
            buffer[9] = valueBytes[7];
            buffer[8] = valueBytes[6];
            buffer[7] = valueBytes[5];
            buffer[6] = valueBytes[4];
            buffer[5] = valueBytes[3];
            buffer[4] = valueBytes[2];
            buffer[3] = valueBytes[1];
            buffer[2] = valueBytes[0];

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to enter timesync routine.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_EnterTimeSync(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_ENTER_TIME_SYNC];

            // Start timesync procedure
            buffer[0] = (byte)Command.CMD_TIME_SYNC;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to exit timesync routine.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>      
        public static byte[] Cmd_ExitTimeSync(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_EXIT_TIME_SYNC];

            // Stop timesync procedure
            buffer[0] = (byte)Command.CMD_EXIT_TIME_SYNC;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to set a custom sensors full scale configuration.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="gyrFS">Gyroscope full scale code.</param>
        /// <param name="axlFS">Accelerometer full scale code.</param>
        /// <param name="magFS">Magnetometer full scale code.</param>
        /// <param name="hdrFS">High Dynamic Range (HDR) Accelerometer code.</param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        public static byte[] Cmd_SetSensorsFullScale(GyroscopeFS gyrFS, AccelerometerFS axlFS, MagnetometerFS magFS, AccelerometerHDRFS hdrFS, CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_SET_SENSORS_FS];

            // Set sensors full scale configuration
            buffer[0] = (byte)Command.CMD_SENSORS_FS;
            buffer[1] = (byte)CommandLength.CMD_LENGTH_SET_SENSORS_FS - 2;

            // Build integer configuration code to be sent
            buffer[2] = (byte)((byte)axlFS | (byte)gyrFS | (byte)hdrFS | (byte)magFS);

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to retrieve the current sensors full scale configuration.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>   
        public static byte[] Cmd_GetSensorsFullScale(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_SENSORS_FS];

            // Get current sensors full scale configuration
            buffer[0] = (byte)Command.CMD_SENSORS_FS + READ_BIT_MASK;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        // CMD_CALIB_MATRIX = 0x48
        public static byte[] Cmd_SetCalibrationMatrix(byte rowId, byte colId, float r1, float r2, float r3, CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_SET_CALIB_MATRIX];

            // Set calibration matrix components
            buffer[0] = (byte)Command.CMD_CALIB_MATRIX;
            buffer[1] = (byte)CommandLength.CMD_LENGTH_SET_CALIB_MATRIX - 2;

            // Payload
            buffer[2] = rowId;                                  // Row index in the range 0-2
            buffer[3] = colId;                                  // Column index in the range 0-3

            // Values for a given row and col
            byte[] valByteArray = BitConverter.GetBytes(r1);
            Array.Copy(valByteArray, 0, buffer, 4, 4);          // Value 1 - row 0
            valByteArray = BitConverter.GetBytes(r2);
            Array.Copy(valByteArray, 0, buffer, 8, 4);          // Value 2 - row 1
            valByteArray = BitConverter.GetBytes(r3);
            Array.Copy(valByteArray, 0, buffer, 12, 4);         // Value 3 - row 2

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        public static byte[] Cmd_GetCalibrationMatrix(byte rowId, byte colId, CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_CALIB_MATRIX];

            // Get current calibration matrix values
            buffer[0] = (byte)Command.CMD_CALIB_MATRIX + READ_BIT_MASK;
            buffer[1] = (byte)CommandLength.CMD_LENGTH_GET_CALIB_MATRIX - 2;

            // Set row/col payload indexes to be retrieved
            buffer[2] = rowId;
            buffer[3] = colId;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to configure a default log mode controlled using the smart button.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="mode">Identifies the set of data to be acquired.</param>
        /// <param name="frequency">Identifies the data acquisition frequency.</param>
        /// <param name="channel">Communication channel over which the command will be sent.</param> 
        public static byte[] Cmd_SetButtonLogConfiguration(DataMode mode, DataFrequency frequency, CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_SET_BUTTON_LOG];
            
            // Set log mode command
            buffer[0] = (byte)Command.CMD_BUTTON_LOG;
            buffer[1] = (byte)CommandLength.CMD_LENGTH_SET_BUTTON_LOG - 2;

            // Set log mode code
            byte[] tmp = new byte[3];
            tmp = BitConverter.GetBytes((UInt32)mode);
            Array.Copy(tmp, 0, buffer, 2, 3);

            // Set log frequency
            buffer[5] = (byte)frequency;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to retrieve the current log mode.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param> 
        public static byte[] Cmd_GetButtonLogConfiguration(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_BUTTON_LOG];

            // Get current log mode
            buffer[0] = (byte)Command.CMD_BUTTON_LOG + READ_BIT_MASK;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to retrieve the current user configuration parameters.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param> 
        public static byte[] Cmd_GetUserConfiguration(CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_GET_USER_CONFIG];

            // Get current user configuration
            buffer[0] = (byte)Command.CMD_USER_CONFIG + READ_BIT_MASK;

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        /// <summary>Builds command to retrieve the current log mode.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="bitMask">16-bit unsigned integer representing the configuration channels to be set.</param> 
        /// <param name="standby">Boolean flag that allows to enable / disable automatic standby.</param> 
        /// <param name="circular">Boolean flag that allows to enable / disable circular memory management.</param> 
        /// <param name="usbStream">Boolean flag that allows to set default streaming channel to USB.</param> 
        /// <param name="channel">Communication channel over which the command will be sent.</param> 
        public static byte[] Cmd_SetUserConfiguration(UInt16 bitMask, bool standby, bool circular, bool usbStream, CommunicationChannel channel = CommunicationChannel.CHANNEL_BLE)
        {
            // Definition of message buffer
            byte[] buffer = new byte[(int)CommandLength.CMD_LENGTH_SET_USER_CONFIG];

            // Set a custom user configuration
            buffer[0] = (byte)Command.CMD_USER_CONFIG;
            buffer[1] = (byte)CommandLength.CMD_LENGTH_SET_USER_CONFIG - 2;

            // Set bit-mask from UserConfigMask enum
            byte[] tmp = new byte[2];
            tmp = BitConverter.GetBytes((ushort)bitMask);
            Array.Copy(tmp, 0, buffer, 2, 2);

            // Build configuration code to be sent using user input
            ushort config_code = 0;
            if (standby)
                config_code |= 0x0001;

            if (circular)
                config_code |= 0x0002;

            if (usbStream)
                config_code |= 0x0004;

            // Set configuration code
            tmp = BitConverter.GetBytes(config_code);
            Array.Copy(tmp, 0, buffer, 4, 2);

            // Wrap message with header and trailer in the case of USB communication
            if (channel == CommunicationChannel.CHANNEL_USB)
                return WrapMessage(buffer);

            return buffer;
        }

        #endregion

        #region DECODING FUNCTIONS

        /// <summary>Parse command characteristic to get a command response object.</summary>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        /// <param name="buffer">Byte array to be parsed.</param>
        /// <param name="response">CommandResponse (output) object reference.</param>
        public static void ParseCommandCharacteristic(CommunicationChannel channel, byte[] buffer, out CommandResponse response)
        {
            // Build a command response container given the command characteristic content as a byte array
            // Manage also header and trailer removal in case of BLE or USB channel
            if (channel == CommunicationChannel.CHANNEL_BLE)
                response = new CommandResponse(buffer);
            else
                response = new CommandResponse(ExtractMessage(buffer));
        }

        /// <summary>Decode system state.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="state">Output reference to the SystemState value.</param>
        public static void Dec_SystemState(CommandResponse response, out SystemState state)
        {
            state = SystemState.SYS_NONE;

            // Decode system state given command response
            if (((byte)response.tx & 0x7F) == (byte)Command.CMD_STATE &&
                response.ack == AcknowledgeType.ACK_SUCCESS)
            {
                if (((byte)response.tx & READ_BIT_MASK) != 0)
                {
                    state = (SystemState)response.payload[0];
                }
            }
        }

        /// <summary>Decode firmware application information.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="crc">Output reference to the Circular Redundancy Check (CRC) as 32-bit unsigned integer.</param>
        /// <param name="length">Output reference to the length of the application firmware (i.e., number of bytes), as a 32-bit unsigned integer.</param>
        public static void Dec_ApplicationInfo(CommandResponse response, out uint crc, out uint length)
        {
            crc = 0;
            length = 0;

            // Decode firmware application info given command response payload
            if (((byte)response.tx & 0x7F) == (byte)Command.CMD_APP_INFO &&
                response.ack == AcknowledgeType.ACK_SUCCESS)
            {
                if (((byte)response.tx & READ_BIT_MASK) != 0)
                {
                    crc = BitConverter.ToUInt32(response.payload, 0);
                    length = BitConverter.ToUInt32(response.payload, 4);
                }
            }

        }

        /// <summary>Decode battery charge level.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="charge">Output reference to battery charge value [%].</param>
        public static void Dec_BatteryCharge(CommandResponse response, out int charge)
        {
            charge = -1;

            // Decode battery charge percentage value given command response payload
            if (((byte)response.tx & 0x7F) == (byte)Command.CMD_BATTERY_CHARGE &&
                response.ack == AcknowledgeType.ACK_SUCCESS)
            {
                if (((byte)response.tx & READ_BIT_MASK) != 0)
                {
                    charge = response.payload[0];
                }
            }
        }

        /// <summary>Decode battery voltage level.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="voltage">Output reference to battery voltage value [mV].</param>
        public static void Dec_BatteryVoltage(CommandResponse response, out int voltage)
        {
            voltage = -1;

            // Decode battery voltage [mV] value given command response payload
            if (((byte)response.tx & 0x7F) == (byte)Command.CMD_BATTERY_VOLTAGE &&
                response.ack == AcknowledgeType.ACK_SUCCESS)
            {
                if (((byte)response.tx & READ_BIT_MASK) != 0)
                {
                    voltage = BitConverter.ToUInt16(response.payload,0);
                }
            }
        }

        /// <summary>Decode check-up register code.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="code">Output reference to check-up register code.</param>
        public static void Dec_CheckUp(CommandResponse response, out string code)
        {
            // Decode checkup register value, as string, given the command response payload
            string tmpOut = "";

            // Decode current checkup register value given command response payload
            if (((byte)response.tx & 0x7F) == (byte)Command.CMD_CHECK_UP &&
                response.ack == AcknowledgeType.ACK_SUCCESS)
            {
                if (((byte)response.tx & READ_BIT_MASK) != 0)
                {
                    for (int i = 0; i < response.len - 2; i++)
                        tmpOut += response.payload[i].ToString("X2") + " ";
                }
            }

            code = tmpOut;
        }

        /// <summary>Decode firmware version labels.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="boot_rev">Output reference to boot loader firmware version label.</param>
        /// <param name="app_rev">Output reference to application firmware version label.</param>
        public static void Dec_FirmwareVersion(CommandResponse response, out string boot_rev, out string app_rev)
        {
            boot_rev = "x.x.x";
            app_rev = "x.x.x";

            // Decode current firmware versions given command response payload
            if (((byte)response.tx & 0x7F) == (byte)Command.CMD_FW_VERSION &&
                response.ack == AcknowledgeType.ACK_SUCCESS)
            {
                if (((byte)response.tx & READ_BIT_MASK) != 0)
                {
                    // Decode bootloader and application firmware versions
                    string[] words = System.Text.Encoding.UTF8.GetString(response.payload).Replace("\0", " ").Split();

                    boot_rev = words[0];
                    app_rev = words[1];
                }
            }
        }

        /// <summary>Decode date/time.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="datetime">Output reference to date/time object.</param>
        public static void Dec_DateTime(CommandResponse response, out DateTime datetime)
        {
            datetime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            // Decode current Date/Time given command response payload
            if (((byte)response.tx & 0x7F) == (byte)Command.CMD_TIME &&
                response.ack == AcknowledgeType.ACK_SUCCESS)
            {
                if (((byte)response.tx & READ_BIT_MASK) != 0)
                {
                    var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    datetime = epoch.AddSeconds(BitConverter.ToUInt32(response.payload, 0));
                }
            }
        }

        /// <summary>Decode device name (i.e., it is the name advertised by Bluetooth module).</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="name">Output reference to device name.</param>
        public static void Dec_DeviceName(CommandResponse response, out string name)
        {
            name = "-";

            // Decode current device name (i.e., ble name) given command response payload
            if (((byte)response.tx & 0x7F) == (byte)Command.CMD_BLE_NAME &&
                response.ack == AcknowledgeType.ACK_SUCCESS)
            {
                if (((byte)response.tx & READ_BIT_MASK) != 0)
                {
                    name = System.Text.Encoding.ASCII.GetString(response.payload);
                }
            } 
        }

        /// <summary>Decode device unique identifier.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="id">Output reference to unique identifier.</param>
        public static void Dec_DeviceID(CommandResponse response, out string id)
        {
            id = "";

            // Decode current device unique identifier given command response payload
            if (((byte)response.tx & 0x7F) == (byte)Command.CMD_DEVICE_ID &&
                response.ack == AcknowledgeType.ACK_SUCCESS)
            {
                if (((byte)response.tx & READ_BIT_MASK) != 0)
                {
                    // id = BitConverter.ToUInt32(response.payload, 0).ToString("X8");
                    // Revert array before printing string in order to keep endianness in byte representation
                    Array.Reverse(response.payload);
                    for (int i = 0; i < response.len - 2; i++)
                        id += response.payload[i].ToString("X2");
                }
            } 
        }

        /// <summary>Decode device skills (i.e., hardware features provided by the device).</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="skills">Output reference to hardware skills.</param>
        public static void Dec_DeviceSkills(CommandResponse response, out List<KeyValuePair<UInt32, string>> skills)
        {
            skills = new List<KeyValuePair<UInt32, string>>();

            // Get skills code to be parsed
            UInt32 skillsCode = BitConverter.ToUInt32(response.payload,0);

            // Extract hardware skills given the overall skills code
            if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_GYRO) > 0)
                skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_GYRO, "GYR"));
            if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_AXL) > 0)
                skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_AXL, "AXL"));
            if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_MAGN) > 0)
                skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_MAGN, "MAG"));
            if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_HDR) > 0)
                skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_HDR, "HDR"));
            if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_TEMP) > 0)
                skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_TEMP, "TEMP"));
            if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_RH) > 0)
                skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_RH, "RH"));
            if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_BAR) > 0)
                skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_BAR, "BAR"));
            if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_LUM_VIS) > 0)
                skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_LUM_VIS, "LUM/VIS"));
            if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_LUM_IR) > 0)
                skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_LUM_IR, "LUM/IR"));
            if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_RANGE) > 0)
                skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_RANGE, "RANGE"));
            if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_MIC) > 0)
                skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_MIC, "MIC"));
        }

        /// <summary>Decode memory status information.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="available_memory">Output reference to available memory.</param>
        /// <param name="number_of_files">Output reference to number of files currently saved in memory.</param>
        public static void Dec_MemoryStatus(CommandResponse response, out uint available_memory, out uint number_of_files)
        {
            available_memory = 100;
            number_of_files = 0;

            // Decode current memory status given command response payload
            if (((byte)response.tx & 0x7F) == (byte)Command.CMD_MEM_CONTROL &&
                response.ack == AcknowledgeType.ACK_SUCCESS)
            {
                if (((byte)response.tx & READ_BIT_MASK) != 0)
                {
                    // Get available free memory (i.e., percentage value)
                    available_memory = (uint)response.payload[0];

                    // Get number of files currently saved in memory
                    number_of_files = BitConverter.ToUInt16(response.payload, 1);
                }
            }
        }

        /// <summary>Decode file information.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="file_info">Output reference to a FileInfo structure.</param>
        public static void Dec_FileInfo(CommandResponse response, out FileInfo file_info)
        {
            file_info = new FileInfo();

            // Decode current file info given command response payload
            if (((byte)response.tx & 0x7F) == (byte)Command.CMD_MEM_FILE_INFO &&
                response.ack == AcknowledgeType.ACK_SUCCESS)
            {
                if (((byte)response.tx & READ_BIT_MASK) != 0)
                {
                    // Decode file timestamp
                    byte[] tmp = new byte[8];
                    Array.Copy(response.payload, 0, tmp, 0, 5);
                    UInt64 ts = BitConverter.ToUInt64(tmp, 0);
                    ts += ((UInt64)REFERENCE_EPOCH) * 1000;

                    // Decode sensors full scales

                    // Pad 1-bytes response with further 3-bytes before converting to UInt32 and extracting configuration codes
                    tmp = new byte[4];
                    Array.Copy(response.payload, 5, tmp, 0, 1);
                    UInt32 code = BitConverter.ToUInt32(tmp, 0);

                    SensorConfig gyrConfig, axlConfig, magConfig, hdrConfig;
                    // DecodeMEMSConfiguration(response.payload[5], out gyrConfig, out axlConfig, out magConfig, out hdrConfig);
                    DecodeMEMSConfiguration(code, out gyrConfig, out axlConfig, out magConfig, out hdrConfig);

                    // Decode data acquisition mode
                    tmp = new byte[4];
                    Array.Copy(response.payload, 6, tmp, 0, 3);
                    UInt32 dm = BitConverter.ToUInt32(tmp, 0);

                    // Update file_info object
                    file_info = new FileInfo(ts, gyrConfig, axlConfig, magConfig, hdrConfig, dm, response.payload[9]);
                }
            }
        }

        public static void Dec_ClockOffset(CommandResponse response, out UInt64 clock_offset)
        {
            clock_offset = 0;

            // Decode current file info given command response payload
            if (((byte)response.tx & 0x7F) == (byte)Command.CMD_CLK_OFFSET &&
                response.ack == AcknowledgeType.ACK_SUCCESS)
            {
                if (((byte)response.tx & READ_BIT_MASK) != 0)
                {
                    clock_offset = BitConverter.ToUInt64(response.payload, 0);
                }
            }
        }

        /// <summary>Decode sensors full scale / sensitivity configuration.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="gyrConfig">Output reference to Gyroscope configuration.</param>
        /// <param name="axlConfig">Output reference to Accelerometer configuration.</param>
        /// <param name="magConfig">Output reference to Magnetometer configuration.</param>
        /// <param name="hdrConfig">Output reference to High Dynamic Range (HDR) Accelerometer configuration.</param>
        public static void Dec_SensorFullScales(CommandResponse response, out SensorConfig gyrConfig, out SensorConfig axlConfig, out SensorConfig magConfig, out SensorConfig hdrConfig)
        {
            gyrConfig = new SensorConfig();
            axlConfig = new SensorConfig();
            magConfig = new SensorConfig();
            hdrConfig = new SensorConfig();

            // Decode current sensors full scales (i.e., gyr / axl / hdr / mag) given command response payload
            if (((byte)response.tx & 0x7F) == (byte)Command.CMD_SENSORS_FS &&
                response.ack == AcknowledgeType.ACK_SUCCESS)
            {
                if (((byte)response.tx & READ_BIT_MASK) != 0)
                {
                    // Pad 3-bytes response.payload array with a further byte before converting to UInt32 and extracting configuration codes
                    byte[] tmp = new byte[4];
                    Array.Copy(response.payload, tmp, 3);
                    UInt32 code = BitConverter.ToUInt32(tmp, 0);

                    // Extract MEMS configurations
                    // DecodeMEMSConfiguration(response.payload[0], out gyrConfig, out axlConfig, out magConfig, out hdrConfig);
                    DecodeMEMSConfiguration(code, out gyrConfig, out axlConfig, out magConfig, out hdrConfig);
                }
            }
        }

        public static float[] Dec_CalibrationMatrixValues(byte[] colVal)
        {
            float[] colValues = new float[3];

            byte[] tmp = new byte[4];
            Array.Copy(colVal, 0, tmp, 0, 4);
            colValues[0] = (float)BitConverter.ToInt32(tmp, 0);
            Array.Copy(colVal, 4, tmp, 0, 4);
            colValues[1] = (float)BitConverter.ToInt32(tmp, 0);
            Array.Copy(colVal, 8, tmp, 0, 4);
            colValues[2] = (float)BitConverter.ToInt32(tmp, 0);

            return colValues;
        }

        /// <summary>Decode button log configuration.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="mode">Output reference to current acquisition mode configured.</param>
        /// <param name="frequency">Output reference to current acquisition frequency configured.</param>
        public static void Dec_ButtonLogConfiguration(CommandResponse response, out string mode, out string frequency)
        {
            mode = "";
            frequency = "";

            // Decode current sensors full scales (i.e., gyr / axl / hdr / mag) given command response payload
            if (((byte)response.tx & 0x7F) == (byte)Command.CMD_BUTTON_LOG &&
                response.ack == AcknowledgeType.ACK_SUCCESS)
            {
                if (((byte)response.tx & READ_BIT_MASK) != 0)
                {
                    // Pad 3-bytes response.payload array with a further byte before converting to UInt32
                    byte[] tmp = new byte[4];
                    Array.Copy(response.payload, tmp, 3);

                    // Apply bitwise mask to get data acquisition mode combination
                    UInt32 code = BitConverter.ToUInt32(tmp, 0);

                    // Build acquisition mode string description
                    mode = DataModeToString(code);

                    // Set acquisition frequency string representation
                    frequency = ((DataFrequency)response.payload[3]).ToString();

                }
            }
        }

        /// <summary>Decode user configuration packet.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="userConfig">Output reference to user configuration object.</param>
        public static void Dec_UserConfiguration(CommandResponse response, out UserConfig userConfig)
        {
            userConfig = new UserConfig();

            // Get code from which the user configuration must be extracted
            UInt16 code = BitConverter.ToUInt16(response.payload, 0);

            // Set internal boolean flags
            bool standby = false;
            bool memory = false;
            bool usb = false;

            if ((code & (UInt32)UserConfigMask.USER_CFG_MASK_USB_STREAM) > 0)
                usb = true;
            if ((code & (UInt32)UserConfigMask.USER_CFG_MASK_CIRCULAR_MEMORY) > 0)
                memory = true;
            if ((code & (UInt32)UserConfigMask.USER_CFG_MASK_STANDBY) > 0)
                standby = true;

            userConfig = new UserConfig(standby, memory, usb);
        }

        /// <summary>Returns the data packet dimension corresponding to a given data acquisition mode.</summary>
        /// <returns>A integer value representing the data packet dimension.</returns>
        /// <param name="inMode">Data acquisition mode.</param>
        public static int GetPacketDimension(DataMode inMode)
        {
            int packet_dimension = 0;

            // Compute packet dimension given data acquisition mode
            if ((inMode & DataMode.DATA_MODE_GYRO) > 0)
                packet_dimension += (int)DataSize.DATA_SIZE_GYRO;
            if ((inMode & DataMode.DATA_MODE_AXL) > 0)
                packet_dimension += (int)DataSize.DATA_SIZE_AXL;
            if ((inMode & DataMode.DATA_MODE_HDR) > 0)
                packet_dimension += (int)DataSize.DATA_SIZE_HDR;
            if ((inMode & DataMode.DATA_MODE_MAGN) > 0)
                packet_dimension += (int)DataSize.DATA_SIZE_MAGN;
            if ((inMode & DataMode.DATA_MODE_ORIENTATION) > 0)
                packet_dimension += (int)DataSize.DATA_SIZE_ORIENTATION;
            if ((inMode & DataMode.DATA_MODE_TIMESTAMP) > 0)
                packet_dimension += (int)DataSize.DATA_SIZE_TIMESTAMP;
            if ((inMode & DataMode.DATA_MODE_TEMP_HUM) > 0)
                packet_dimension += (int)DataSize.DATA_SIZE_TEMP_HUM;
            if ((inMode & DataMode.DATA_MODE_TEMP_PRESS) > 0)
                packet_dimension += (int)DataSize.DATA_SIZE_TEMP_PRESS;
            if ((inMode & DataMode.DATA_MODE_RANGE) > 0)
                packet_dimension += (int)DataSize.DATA_SIZE_RANGE;
            if ((inMode & DataMode.DATA_MODE_SOUND) > 0)
                packet_dimension += (int)DataSize.DATA_SIZE_SOUND;

            // Check packet dimension consistency
            // It MUST be a dividend of 120 (2040)
            if (packet_dimension == 6 || packet_dimension == 12 || packet_dimension == 24 ||
                    packet_dimension == 30 || packet_dimension == 60)
                return packet_dimension;

            // Return -1 in case of data dimension inconsistency
            return -1;
        }

        /// <summary>Returns the overall number of data packets contained into a 128-bytes data characteristic given a data acquisition mode.</summary>
        /// <returns>A integer value representing the number of data packets.</returns>
        /// <param name="mode">Data acquisition mode.</param>
        public static int GetNumberOfPackets(DataMode mode)
        {
            // Compute packet dimension and check its consistency given data acquisition mode
            int packet_dimension = GetPacketDimension(mode);

            // Compute overall number of packets contained into a data characteristic update
            if (packet_dimension != -1)
                return (120 / packet_dimension);

            // Return -1 in case of data dimension inconsistency
            return -1;
        }

        /// <summary>Parse data characteristic to get an observable collection of muse data objects.</summary>
        /// <returns>An observable collection of muse data objects.</returns>
        /// <param name="mode">Data acquisition mode.</param>
        /// <param name="type">Data acquisition type.</param>
        /// <param name="buffer">Byte array to be parsed.</param>
        /// <param name="overall_packet_dimension">Overal packet dimension based on selected acquisition mode.</param>
        /// <param name="num_of_packets">Number of packets into buffer.</param>
        /// <param name="gyr_res">Gyroscope sensitivity coefficient.</param>
        /// <param name="axl_res">Accelerometer sensitivity coefficient.</param>
        /// <param name="mag_res">Magnetometer sensitivity coefficient.</param>
        /// <param name="hdr_res">High Dynamic Range (HDR) Accelerometer sensitivity coefficient.</param>
        public static ObservableCollection<Muse_Data> ParseDataCharacteristic(DataMode mode,
                                    AcquisitionType type,
                                    byte[] buffer,
                                    int overall_packet_dimension,
                                    int num_of_packets,
                                    float gyr_res, float axl_res, float mag_res, float hdr_res)
        {
            if (buffer != null && buffer.Length > 0)
            {
                // Set decoded data buffer to be returned
                ObservableCollection<Muse_Data> data_collection = new ObservableCollection<Muse_Data>();

                // Manage data decoding based on selected acquisition type
                if (type == AcquisitionType.ACQ_READOUT)
                {
                    ///////////////////////////////////////////////////
                    // Manage file offload decoding

                }
                else
                {
                    ///////////////////////////////////////////////////
                    // Decode timestamp from raw data header (5 bytes)
                    byte[] currentTime = new byte[8];
                    Array.Copy(buffer, 0, currentTime, 0, 5);
                    UInt64 currentTime_val = BitConverter.ToUInt64(currentTime, 0);
                    currentTime_val += (REFERENCE_EPOCH * 1000);

                    // Initially equal to the default length of the message header
                    int overall_index_offset = 8;

                    ///////////////////////////////////////////////////////////////////////
                    // Manage stream decoding based on BUFFERED or DIRECT acquisition type
                    if (type == AcquisitionType.ACQ_TX_BUFFERED)
                    {
                        for (int i = 0; i < num_of_packets; i++)
                        {
                            // Iterate through the buffer and parse data packets
                            byte[] rawPacket = new byte[overall_packet_dimension];
                            Array.Copy(buffer, overall_index_offset, rawPacket, 0, overall_packet_dimension);

                            // Decode current data packet
                            Muse_Data current_data = DecodePacket(rawPacket, currentTime_val, mode, gyr_res, axl_res, mag_res, hdr_res);
                            data_collection.Add(current_data);

                            // Update overall index offset 
                            overall_index_offset += overall_packet_dimension;
                        }
                    }
                    else if (type == AcquisitionType.ACQ_TX_DIRECT)
                    {
                        // Retrieve the overall payload
                        byte[] rawPacket = new byte[overall_packet_dimension];
                        Array.Copy(buffer, overall_index_offset, rawPacket, 0, overall_packet_dimension);

                        // Decode current data packet
                        Muse_Data current_data = DecodePacket(rawPacket, currentTime_val, mode, gyr_res, axl_res, mag_res, hdr_res);
                        data_collection.Add(current_data);
                    }
                }

                return data_collection;
            }

            return null;
        }

        /// <summary>Decode data packet.</summary>
        /// <returns>A muse data object.</returns>
        /// <param name="buffer">Byte array to be decoded.</param>
        /// <param name="timestamp">Reference timestamp related to the last notification.</param>
        /// <param name="mode">Data acquisition mode.</param>
        /// <param name="gyr_res">Gyroscope sensitivity coefficient.</param>
        /// <param name="axl_res">Accelerometer sensitivity coefficient.</param>
        /// <param name="mag_res">Magnetometer sensitivity coefficient.</param>
        /// <param name="hdr_res">High Dynamic Range (HDR) Accelerometer sensitivity coefficient.</param>
        private static Muse_Data DecodePacket(byte[] buffer, ulong timestamp, DataMode mode, float gyr_res, float axl_res, float mag_res, float hdr_res)
        {
            // Check input buffer consistency
            if (buffer != null && buffer.Length > 0)
            {
                // Define muse data container
                int packet_index_offset = 0;
                Muse_Data current_data = new Muse_Data();

                // Set overall timestamp reference
                current_data.OverallTimestamp = timestamp;

                // Ther order of IF statements is related to the order with which the data coming within each packet in order to properly update the packet_index_offset
                if ((mode & DataMode.DATA_MODE_GYRO) > 0)
                {
                    // Decode Gyroscope reading
                    byte[] currentPacket = new byte[(int)DataSize.DATA_SIZE_GYRO];
                    Array.Copy(buffer, packet_index_offset, currentPacket, 0, (int)DataSize.DATA_SIZE_GYRO);
                    packet_index_offset += (int)DataSize.DATA_SIZE_GYRO;

                    current_data.Gyroscope = DataTypeGYR(currentPacket, gyr_res);
                }

                if ((mode & DataMode.DATA_MODE_AXL) > 0)
                {
                    // Decode Accelerometer reading
                    byte[] currentPacket = new byte[(int)DataSize.DATA_SIZE_AXL];
                    Array.Copy(buffer, packet_index_offset, currentPacket, 0, (int)DataSize.DATA_SIZE_AXL);
                    packet_index_offset += (int)DataSize.DATA_SIZE_AXL;

                    current_data.Accelerometer = DataTypeAXL(currentPacket, axl_res);
                }

                if ((mode & DataMode.DATA_MODE_MAGN) > 0)
                {
                    // Decode Magnetometer reading
                    byte[] currentPacket = new byte[(int)DataSize.DATA_SIZE_MAGN];
                    Array.Copy(buffer, packet_index_offset, currentPacket, 0, (int)DataSize.DATA_SIZE_MAGN);
                    packet_index_offset += (int)DataSize.DATA_SIZE_MAGN;

                    current_data.Magnetometer = DataTypeMAGN(currentPacket, mag_res);
                }

                if ((mode & DataMode.DATA_MODE_HDR) > 0)
                {
                    // Decode Accelerometer HDR reading
                    byte[] currentPacket = new byte[(int)DataSize.DATA_SIZE_HDR];
                    Array.Copy(buffer, packet_index_offset, currentPacket, 0, (int)DataSize.DATA_SIZE_HDR);
                    packet_index_offset += (int)DataSize.DATA_SIZE_HDR;

                    current_data.HDR = DataTypeHDR(currentPacket, hdr_res);
                }

                if ((mode & DataMode.DATA_MODE_ORIENTATION) > 0)
                {
                    // Decode orientation quaternion
                    byte[] currentPacket = new byte[(int)DataSize.DATA_SIZE_ORIENTATION];
                    Array.Copy(buffer, packet_index_offset, currentPacket, 0, (int)DataSize.DATA_SIZE_ORIENTATION);
                    packet_index_offset += (int)DataSize.DATA_SIZE_ORIENTATION;

                    current_data.Quaternion = DataTypeOrientation(currentPacket);
                    current_data.Euler = GetAnglesFromQuaternion(current_data.Quaternion);
                }

                if ((mode & DataMode.DATA_MODE_TIMESTAMP) > 0)
                {
                    // Decode timestamp
                    byte[] currentPacket = new byte[(int)DataSize.DATA_SIZE_TIMESTAMP];
                    Array.Copy(buffer, packet_index_offset, currentPacket, 0, (int)DataSize.DATA_SIZE_TIMESTAMP);
                    packet_index_offset += (int)DataSize.DATA_SIZE_TIMESTAMP;

                    current_data.Timestamp = DataTypeTimestamp(currentPacket);
                }

                if ((mode & DataMode.DATA_MODE_TEMP_HUM) > 0)
                {
                    // Decode temperature and humidity reading
                    byte[] currentPacket = new byte[(int)DataSize.DATA_SIZE_TEMP_HUM];
                    Array.Copy(buffer, packet_index_offset, currentPacket, 0, (int)DataSize.DATA_SIZE_TEMP_HUM);
                    packet_index_offset += (int)DataSize.DATA_SIZE_TEMP_HUM;

                    current_data.TH = DataTypeTempHum(currentPacket);
                }

                if ((mode & DataMode.DATA_MODE_TEMP_PRESS) > 0)
                {
                    // Decode temperature and barometric pressure reading
                    byte[] currentPacket = new byte[(int)DataSize.DATA_SIZE_TEMP_PRESS];
                    Array.Copy(buffer, packet_index_offset, currentPacket, 0, (int)DataSize.DATA_SIZE_TEMP_PRESS);
                    packet_index_offset += (int)DataSize.DATA_SIZE_TEMP_PRESS;

                    current_data.TP = DataTypeTempPress(currentPacket);
                }

                if ((mode & DataMode.DATA_MODE_RANGE) > 0)
                {
                    // Decode distance / luminosity reading
                    byte[] currentPacket = new byte[(int)DataSize.DATA_SIZE_RANGE];
                    Array.Copy(buffer, packet_index_offset, currentPacket, 0, (int)DataSize.DATA_SIZE_RANGE);
                    packet_index_offset += (int)DataSize.DATA_SIZE_RANGE;

                    current_data.Range = DataTypeRange(currentPacket);
                }

                if ((mode & DataMode.DATA_MODE_SOUND) > 0)
                {
                    // Decode sound reading
                    byte[] currentPacket = new byte[(int)DataSize.DATA_SIZE_SOUND];
                    Array.Copy(buffer, packet_index_offset, currentPacket, 0, (int)DataSize.DATA_SIZE_SOUND);
                    // packet_index_offset += (int)DataSize.DATA_SIZE_SOUND;

                    current_data.Sound = DataTypeSound(currentPacket);
                }

                return current_data;
            }
            return null;
        }

        /// <summary>Decode Gyroscope reading.</summary>
        /// <returns>An array of float (i.e., x, y, z axes).</returns>
        /// <param name="currentPayload">Bytes array to be decoded.</param>
        /// <param name="gyr_res">Gyroscope sensitivity coefficient.</param>
        private static float[] DataTypeGYR(byte[] currentPayload, float gyr_res)
        {
            // Define 3-elements array of float (i.e., x, y, z channels)
            float[] currentData = new float[3];
            // Define temporary internal variable used for data conversion
            byte[] tmp = new byte[2];

            // Iterate across Gyroscope channels
            for (int i = 0; i < 3; i++)
            {
                // Extract channel raw value (i.e., 2-bytes each)
                Array.Copy(currentPayload, 2*i, tmp, 0, 2);
                // Convert to Int16 and apply sensitivity scaling
                currentData[i] = (float)(BitConverter.ToInt16(tmp, 0) * gyr_res);
            }

            // Return decoded Gyroscope reading
            return currentData;
        }

        /// <summary>Decode Accelerometer reading.</summary>
        /// <returns>An array of float (i.e., x, y, z axes).</returns>
        /// <param name="currentPayload">Bytes array to be decoded.</param>
        /// <param name="axl_res">Accelerometer sensitivity coefficient.</param>
        private static float[] DataTypeAXL(byte[] currentPayload, float axl_res)
        {
            float[] currentData = new float[3];
            byte[] tmp = new byte[2];

            // Accelerometer
            for (int i = 0; i < 3; i++)
            {
                Array.Copy(currentPayload, 2 * i, tmp, 0, 2);
                currentData[i] = (float)(BitConverter.ToInt16(tmp, 0) * axl_res);
            }

            return currentData;
        }

        /// <summary>Decode Magnetometer reading.</summary>
        /// <returns>An array of float (i.e., x, y, z axes).</returns>
        /// <param name="currentPayload">Bytes array to be decoded.</param>
        /// <param name="mag_res">Accelerometer sensitivity coefficient.</param>
        private static float[] DataTypeMAGN(byte[] currentPayload, float mag_res)
        {
            float[] currentData = new float[3];
            byte[] tmp = new byte[2];

            // Magnetometer
            for (int i = 0; i < 3; i++)
            {
                Array.Copy(currentPayload, 2 * i, tmp, 0, 2);
                currentData[i] = (float)(BitConverter.ToInt16(tmp, 0) * mag_res);
            }

            return currentData;
        }

        /// <summary>Decode High Dynamic Range (HDR) Accelerometer reading.</summary>
        /// <returns>An array of float (i.e., x, y, z axes).</returns>
        /// <param name="currentPayload">Bytes array to be decoded.</param>
        /// <param name="hdr_res">Accelerometer sensitivity coefficient.</param>
        private static float[] DataTypeHDR(byte[] currentPayload, float hdr_res)
        {
            float[] currentData = new float[3];
            byte[] tmp = new byte[2];

            // Accelerometer HDR
            for (int i = 0; i < 3; i++)
            {
                Array.Copy(currentPayload, 2 * i, tmp, 0, 2);
                currentData[i] = (float)((BitConverter.ToInt16(tmp, 0) / 16) * hdr_res);
            }

            return currentData;
        }

        /// <summary>Decode orientation (unit) quaternion.</summary>
        /// <returns>An array of float (i.e., qw, qi, qj, qk).</returns>
        /// <param name="currentPayload">Bytes array to be decoded.</param>
        private static float[] DataTypeOrientation(byte[] currentPayload)
        {
            // Define 4-elements array of float (i.e., qw, qi, qj, qk channels)
            float[] currentData = new float[4];

            // Define temporary internal variable used for data conversion
            byte[] tmp = new byte[2];

            // Orientation Quaternion (i.e., UNIT QUATERNION)
            float[] tempFloat = new float[3];
            for (int i = 0; i < 3; i++)
            {
                // Extract channel raw value (i.e., 2-bytes each) and convert to Int16
                tempFloat[i] = BitConverter.ToInt16(currentPayload, 2*i);

                // Keep into account the data resolution
                tempFloat[i] /= 32767;
                
                // Assign imaginary parts of quaternion
                currentData[i + 1] = tempFloat[i];
            }

            // Compute real component of quaternion given immaginary parts
            currentData[0] = (float)Math.Sqrt(1 - (currentData[1] * currentData[1] +
                    currentData[2] * currentData[2] + currentData[3] * currentData[3]));

            // Return decoded Unit Quaternion
            return currentData;
        }

        /// <summary>Compute Euler Angles given a unit quaternion.</summary>
        /// <returns>An array of float (i.e., roll, pitch and yaw).</returns>
        /// <param name="q">Unit quaternion to be converted.</param>
        private static float[] GetAnglesFromQuaternion(float[] q)
        {
            float[] result = new float[3];

            // Compute Euler Angles given a unit quaternion
            result[0] = (float)(Math.Atan2(2 * (q[0] * q[1] + q[2] * q[3]), 1 - 2 * (q[1] * q[1] + q[2] * q[2])) * 180 / Math.PI);
            result[1] = (float)(Math.Asin(2 * q[0] * q[2] - 2 * q[3] * q[1]) * 180 / Math.PI);
            result[2] = (float)(Math.Atan2(2 * (q[0] * q[3] + q[1] * q[2]), 1 - 2 * (q[2] * q[2] + q[3] * q[3])) * 180 / Math.PI);
            
            return result;
        }

        /// <summary>Decode timestamp.</summary>
        /// <returns>A unsigned long integer value representing the timestamp in epoch format.</returns>
        /// <param name="currentPayload">Bytes array to be decoded.</param>
        private static ulong DataTypeTimestamp(byte[] currentPayload)
        {
            // Set current data variable to 0
            ulong currentData = 0;

            // Get raw byte array representation to be decoded
            byte[] tmp = new byte[8];
            Array.Copy(currentPayload, 0, tmp, 0, 6);
            
            // Convert raw data to 64-unsigned integer representation
            UInt64 tempTime = (ulong)BitConverter.ToUInt64(tmp, 0);
            tempTime &= 0x0000FFFFFFFFFFFF;

            // Add reference epoch
            tempTime += ((UInt64)REFERENCE_EPOCH) * 1000;

            currentData = tempTime;

            // Return current date/time
            return currentData;
        }

        /// <summary>Decode temperature and humidity reading.</summary>
        /// <returns>An array of float (i.e., temperature, humidity).</returns>
        /// <param name="currentPayload">Bytes array to be decoded.</param>
        private static float[] DataTypeTempHum(byte[] currentPayload)
        {
            float[] currentData = new float[2];

            // Temperature
            byte[] tmp = new byte[2];
            Array.Copy(currentPayload, 0, tmp, 0, 2);
            currentData[0] = (float)(BitConverter.ToUInt16(tmp, 0)) * 0.002670f - 45;

            // Humidity
            Array.Copy(currentPayload, 2, tmp, 0, 2);
            currentData[1] = (float)(BitConverter.ToUInt16(tmp, 0)) * 0.001907f - 6;

            return currentData;

        }

        /// <summary>Decode temperature and barometric pressure reading.</summary>
        /// <returns>An array of float (i.e., temperature, pressure).</returns>
        /// <param name="currentPayload">Bytes array to be decoded.</param>
        private static float[] DataTypeTempPress(byte[] currentPayload)
        {
            float[] currentData = new float[2];

            // Temperature
            byte[] tmp = new byte[2];
            Array.Copy(currentPayload, 3, tmp, 0, 2);
            currentData[0] = (float)(BitConverter.ToUInt16(tmp, 0)) / 100;

            // Pressure
            tmp = new byte[3];
            Array.Copy(currentPayload, 0, tmp, 0, 3);
            currentData[1] = (float)(BitConverter.ToUInt16(tmp, 0)) / 4096;

            return currentData;
        }

        private static Range DataTypeRange(byte[] currentPayload)
        {
            Range currentData = new Range(0,0,0);

            return currentData;
        }

        private static float DataTypeSound(byte[] currentPayload)
        {
            float currentData = 0;

            return currentData;
        }

        #endregion

        /// <summary>Decode sensors configuration in terms of full scale and sensitivity.</summary>
        /// <param name="code">24-bit unsigned integer code.</param>
        /// <param name="gyrConfig">Output reference to Gyroscope configuration.</param>
        /// <param name="axlConfig">Output reference to Accelerometer configuration.</param>
        /// <param name="magConfig">Output reference to Magnetometer configuration.</param>
        /// <param name="hdrConfig">Output reference to HDR Accelerometer configuration.</param>
        public static void DecodeMEMSConfiguration(uint code, out SensorConfig gyrConfig, out SensorConfig axlConfig, out SensorConfig magConfig, out SensorConfig hdrConfig)
        {
            // Apply bitwise mask to get sensor full scale code (i.e., gyr, axl, hdr, mag LSB-order)
            byte gyrCode = (byte)(code & (int)MEMS_FullScaleMask.SENSORSFS_MASK_GYRO);
            byte axlCode = (byte)(code & (int)MEMS_FullScaleMask.SENSORSFS_MASK_AXL);
            byte hdrCode = (byte)(code & (int)MEMS_FullScaleMask.SENSORSFS_MASK_HDR);
            byte magCode = (byte)(code & (int)MEMS_FullScaleMask.SENSORSFS_MASK_MAGN);

            // Gyroscope
            Gyroscope_CFG.TryGetValue(gyrCode, out gyrConfig);
            // Accelerometer
            Accelerometer_CFG.TryGetValue(axlCode, out axlConfig);
            // Magnetometer
            Magnetometer_CFG.TryGetValue(magCode, out magConfig);
            // HDR Accelerometer
            AccelerometerHDR_CFG.TryGetValue(hdrCode, out hdrConfig);
        }

        /// <summary>Create a string representation of data acquisition mode.</summary>
        /// <returns>A string.</returns>
        /// <param name="code">32-bit unsigned integer code.</param>
        public static string DataModeToString(UInt32 code)
        {
            List<string> mdString = new List<string>();

            // Build acquisition mode string description
            if ((code & (UInt32)DataMode.DATA_MODE_GYRO) > 0)
                mdString.Add("GYR");
            if ((code & (UInt32)DataMode.DATA_MODE_AXL) > 0)
                mdString.Add("AXL");
            if ((code & (UInt32)DataMode.DATA_MODE_HDR) > 0)
                mdString.Add("HDR");
            if ((code & (UInt32)DataMode.DATA_MODE_MAGN) > 0)
                mdString.Add("MAG");
            if ((code & (UInt32)DataMode.DATA_MODE_ORIENTATION) > 0)
                mdString.Add("QUAT");
            if ((code & (UInt32)DataMode.DATA_MODE_TIMESTAMP) > 0)
                mdString.Add("TIME");
            if ((code & (UInt32)DataMode.DATA_MODE_TEMP_HUM) > 0)
                mdString.Add("TH");
            if ((code & (UInt32)DataMode.DATA_MODE_TEMP_PRESS) > 0)
                mdString.Add("TP");
            if ((code & (UInt32)DataMode.DATA_MODE_RANGE) > 0)
                mdString.Add("RANGE");
            if ((code & (UInt32)DataMode.DATA_MODE_SOUND) > 0)
                mdString.Add("MIC");

            return string.Join(", ", mdString);
        }

        /// <summary>Create a string representation of data acquisition frequency.</summary>
        /// <returns>A string.</returns>
        /// <param name="frequency">8-bit unsigned integer code.</param>
        public static string DataFrequencyToString(byte frequency)
        {
            switch ((DataFrequency)frequency)
            {
                case DataFrequency.DATA_FREQ_25Hz:
                    return "25 Hz";
                case DataFrequency.DATA_FREQ_50Hz:
                    return "50 Hz";
                case DataFrequency.DATA_FREQ_100Hz:
                    return "100 Hz";
                case DataFrequency.DATA_FREQ_200Hz:
                    return "200 Hz";
                case DataFrequency.DATA_FREQ_400Hz:
                    return "400 Hz";
                case DataFrequency.DATA_FREQ_800Hz:
                    return "800 Hz";
                case DataFrequency.DATA_FREQ_1600Hz:
                    return "1600 Hz";
                default:
                    return "NONE";
            }
        }
    }
}
