﻿using System;

using static _221e.Muse.Muse_HW;

namespace _221e.Muse
{
    /// <summary>Command Response structure</summary>
    public struct CommandResponse
    {
        /// <summary>Acknowledge response command</summary>
        public Muse_HW.Command rx { get; private set; }
        /// <summary>Command on which the acknowledge has been sent</summary>
        public Muse_HW.Command tx { get; private set; }

        /// <summary>Payload length</summary>
        public int len { get; private set; }
        /// <summary>Acknowledge error code</summary>
        public Muse_HW.AcknowledgeType ack { get; private set; }

        /// <summary>Payload content</summary>
        public byte[] payload { get; private set; }

        /// <summary>CommandResponse constructor</summary>
        /// <param name="buffer">Byte array representation of the received command response</param>
        public CommandResponse(byte[] buffer)
        {
            if (buffer != null)
            {
                rx = (Muse_HW.Command)buffer[0];
                tx = (Muse_HW.Command)buffer[2];

                len = buffer[1];
                ack = (Muse_HW.AcknowledgeType)buffer[3];

                payload = new byte[len - 2];
                Array.Copy(buffer, 4, payload, 0, len - 2);
            }
            else
            {
                rx = Muse_HW.Command.CMD_NONE;
                tx = Muse_HW.Command.CMD_NONE;
                len = -1;
                ack = Muse_HW.AcknowledgeType.ACK_NONE;
                payload = null;
            }
        }
    }

    /// <summary>File Information structure</summary>
    public struct FileInfo
    {
        /// <summary>Timestamp</summary>
        public UInt64 Timestamp { get; private set; }

        /// <summary>Gyroscope configuration</summary>
        public SensorConfig GyrConfig { get; private set; }
        /// <summary>Accelerometer configuration</summary>
        public SensorConfig AxlConfig { get; private set; }
        /// <summary>Magnetometer configuration</summary>
        public SensorConfig MagConfig { get; private set; }
        /// <summary>HDR Accelerometer configuration</summary>
        public SensorConfig HDRConfig { get; private set; }

        /// <summary>Acquisition mode</summary>
        public Muse_HW.DataMode Mode { get; private set; }
        /// <summary>String representation of acquisition mode</summary>
        public string ModeString { get; private set; }

        /// <summary>Acquisition frequency</summary>
        public Muse_HW.DataFrequency Frequency { get; private set; }
        /// <summary>String representation of acquisition frequency</summary>
        public string FrequencyString { get; private set; }

        /// <summary>FileInfo constructor</summary>
        /// <param name="timestamp">Timestamp</param>
        /// <param name="gyrConfig">Gyroscope configuration</param>
        /// <param name="axlConfig">Accelerometer configuration</param>
        /// <param name="magConfig">Magnetometer configuration</param>
        /// <param name="hdrConfig">HDR Accelerometer configuration</param>
        /// <param name="mode">Acquisition mode</param>
        /// <param name="frequency">Acquisition frequency</param>
        public FileInfo(UInt64 timestamp, SensorConfig gyrConfig, SensorConfig axlConfig, SensorConfig magConfig, SensorConfig hdrConfig, UInt32 mode, byte frequency)
        {
            Timestamp = timestamp;

            GyrConfig = gyrConfig;
            AxlConfig = axlConfig;
            MagConfig = magConfig;
            HDRConfig = hdrConfig;

            Mode = (DataMode)mode;
            ModeString = Muse_Utils.DataModeToString(mode);

            Frequency = (DataFrequency)frequency;
            FrequencyString = Muse_Utils.DataFrequencyToString(frequency);
        }

        /// <summary>Override of ToString method</summary>
        public override string ToString()
        {
            return Timestamp + ", " + 
                GyrConfig.ToString() + ", " + 
                AxlConfig.ToString() + ", " + 
                MagConfig.ToString() + ", " + 
                HDRConfig.ToString() + ", " + 
                ModeString + ", " + 
                FrequencyString;
        }
    }

    /// <summary>Sensors configuration structure</summary>
    public struct SensorConfig
    {
        /// <summary>Full scale integer value</summary>
        public int FullScale { get; private set; }
        /// <summary>Sensitivity coefficient floating point value</summary>
        public float Sensitivity { get; private set; }

        /// <summary>MEMS configuration object constructor</summary>
        /// <param name="fs">Full scale</param>
        /// <param name="sens">Sensitivity</param>
        public SensorConfig(int fs, float sens)
        {
            FullScale = fs;
            Sensitivity = sens;
        }

        /// <summary>Override of ToString method</summary>
        public override string ToString()
        {
            return FullScale + ", " + Sensitivity;
        }
    }

    public struct UserConfig
    {
        /// <summary>Standby enable/disable flag</summary>
        public bool StandbyEnabled { get; private set; }
        /// <summary>Circular Memory enable/disable flag</summary>
        public bool CircularMemoryEnabled { get; private set; }
        /// <summary>USB stream enable/disable flag</summary>
        public bool USBStreamEnabled { get; private set; }

        /// <summary>UserConfig object constructor</summary>
        /// <param name="standby">Boolean enabled/disabled standby status</param>
        /// <param name="memory">Boolean enabled/disabled circular memory status</param>
        /// <param name="usb">Boolean enabled/disabled USB stream status</param>
        public UserConfig(bool standby, bool memory, bool usb)
        {
            StandbyEnabled = standby;
            CircularMemoryEnabled = memory;
            USBStreamEnabled = usb;
        }

        /// <summary>Override of ToString method</summary>
        public override string ToString()
        {
            return base.ToString();
        }
    }

    public struct Range
    {
        private int lum_vis;
        private int lum_ir;
        private int lux;

        public Range(int arg1, int arg2, int arg3)
        {
            lum_vis = arg1;
            lum_ir = arg2;
            lux = arg3;
        }

        public override string ToString()
        {
            string str = lux + "\t" + lum_vis + "\t" + lum_ir;
            return str;
        }
    }

    public class Muse_Data
    {
        private float[] gyr;
        private float[] axl;
        private float[] mag;
        private float[] hdr;
        private float[] th;
        private float[] tp;
        
        private Range range;
        
        private float sound;
        
        private float[] quat;
        private float[] euler;

        private UInt64 timestamp;
        private UInt64 overall_timestamp;

        public Muse_Data()
        {
            gyr = new float[3];
            axl = new float[3];
            mag = new float[3];
            hdr = new float[3];
            th = new float[2];
            tp = new float[2];
        
            range = new Range(0,0,0);
            
            sound = 0;

            quat = new float[4];
            euler = new float[3];

            timestamp = 0;
            overall_timestamp = 0;
        }

        public float[] Gyroscope
        {
            get { return gyr; }
            set
            {
                gyr[0] = value[0];
                gyr[1] = value[1];
                gyr[2] = value[2];
            }
        }

        public float[] Accelerometer
        {
            get { return axl; }
            set
            {
                axl[0] = value[0];
                axl[1] = value[1];
                axl[2] = value[2];
            }
        }

        public float[] Magnetometer
        {
            get { return mag; }
            set
            {
                mag[0] = value[0];
                mag[1] = value[1];
                mag[2] = value[2];
            }
        }

        public float[] HDR
        {
            get { return hdr; }
            set
            {
                hdr[0] = value[0];
                hdr[1] = value[1];
                hdr[2] = value[2];
            }
        }

        public float[] TH
        {
            get { return th; }
            set
            {
                th[0] = value[0];
                th[1] = value[1];
            }
        }

        public float[] TP
        {
            get { return tp; }
            set
            {
                tp[0] = value[0];
                tp[1] = value[1];
            }
        }

        public Range Range
        {
            get { return range; }
            set
            {
                range = value;
            }
        }

        public float Sound
        {
            get { return sound; }
            set
            {
                sound = value;
            }
        }

        public float[] Quaternion
        {
            get { return quat; }
            set
            {
                quat[0] = value[0];
                quat[1] = value[1];
                quat[2] = value[2];
                quat[3] = value[3];
            }
        }

        public float[] Euler
        {
            get { return euler; }
            set
            {
                euler[0] = value[0];
                euler[1] = value[1];
                euler[2] = value[2];
            }
        }

        public UInt64 Timestamp
        {
            get { return timestamp; }
            set { timestamp = value; }
        }

        public UInt64 OverallTimestamp
        {
            get { return overall_timestamp; }
            set { overall_timestamp = value; }
        }

        public string ChannelsToString(float[] arg)
        {
            string str = "";

            for (int i = 0; i < arg.Length - 1; i++)
                str += arg[i] + "\t";
            str += arg[arg.Length - 1];

            return str;
        }

        public string ChannelsToString(int[] arg)
        {
            string str = "";

            for (int i = 0; i < arg.Length - 1; i++)
                str += arg[i] + "\t";
            str += arg[arg.Length - 1];

            return str;
        }

        public override string ToString()
        {
            string str =
                overall_timestamp.ToString() + "\t" +
                timestamp.ToString() + "\t" +
                ChannelsToString(Gyroscope) + "\t" +
                ChannelsToString(Accelerometer) + "\t" +
                ChannelsToString(Magnetometer) + "\t" +
                ChannelsToString(HDR) + "\t" +
                ChannelsToString(TH) + "\t" +
                ChannelsToString(TP) + "\t" +
                range.ToString() + "\t" +
                ChannelsToString(Quaternion);

            return str;
        }
    }
}
