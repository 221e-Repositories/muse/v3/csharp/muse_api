# Muse Application Programming Interface

MUSE is a low power, miniaturized, wireless multi-sensor logger, incorporating state-of-the-art sensing technology into a compact, robust,
customizable, and easy-to-use solution. MUSE combines inertial and environmental sensors together with on-board flash storage, wireless
connectivity, automated on/off functions and regulated rechargeable power, providing a versatile system for data acquisition in a multipurpose
fashion. It’s a fully certified, safe, production and professional research platform for wearable and connected industrial use cases.
MUSE has been used in a variety of wearable and IoT devices created by global enterprises, innovative startups and world-leading researchers.
It can measure acceleration, angular rates, 3D rotation, magnetic fields, temperature, humidity, proximity, ambient light, ambient pressure and
signals intensity. The platform can run 221e proprietary algorithms and embedded AI software libraries to boost intelligent precision sensing
to the next level.

## Getting started

This project builds a .NET Standard 2.0 Class Library. It has been implemented and tested using Microsoft Visual Studio Community 2022 (64-bit) - Version 17.3.4

To make it easy for you to get started with Muse, download the file and build the Muse API class library.

## Contributing
Feel free to dive in! Open an issue or submit PRs.

## License
GNU General Public License.